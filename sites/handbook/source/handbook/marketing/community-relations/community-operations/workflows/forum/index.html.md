---
layout: handbook-page-toc
title: "Forum workflow"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Users

Our [Community Code of Conduct](/community/contribute/code-of-conduct/) applies to the forum platform. When in doubt, [flag posts](https://meta.discourse.org/t/what-are-flags-and-how-do-they-work/32783) which violate the Code of Conduct or are inappropriate in other ways (spam, advertising, etc.). Moderators can review the posts and take action while the posts are hidden from public viewing.

Discourse provides a general [FAQ](https://forum.gitlab.com/faq) for users.

## Best Practices

- Always be courteous and generous, especially when a user is new to the forum. You can recognize new users (and dormant users) by the blue, temporary banner at the top of their posts.
- Add a welcome message when replying to new users. It could be as simple as "Hi, and welcome to the forum! :smile:" at the start of your post.
- Use the like button (the heart icon) as much as you can, to thank users for their input and to inspire other users to do the same.
- Use the solution checkbox button as much as you can, to indicate to others when a forum topic has an answer.
- Thank users publicly for providing answers to questions, especially when they are not in the GitLab Staff group.

## Administration

Most administrative tasks will be done from the [Discourse Admin Dashboard](https://forum.gitlab.com/admin)

### How to Grant Admin Permissions

To add an Admin:

1. Go to the [list of forum users](https://forum.gitlab.com/admin/users/list/active)
1. Use the search box to find the user you want to grant admin permissions to
1. Click on the user to modify their profile
1. Scroll down to the `Permissions` section
1. Click on the `Grant Admin` button
1. An e-mail confirmation will be sent to the Admin that granted the permission (i.e. you). Go to your inbox and click on the link to confirm granting Admin permission to the user
1. If all went well, the `Permissions` > `Admin?` section on the user's profile admin should read `Yes`

Note: we would like to reserve only one or two Staff spots for forum Admins within our existing 15 total Staff spots.

### How to Grant Moderator Permissions

If you want to add an Moderator:

1. Go to the [list of forum users](https://forum.gitlab.com/admin/users/list/active)
1. Use the search box to find the user you want to grant moderator permissions to
1. Click on the user to modify their profile
1. Scroll down to the `Permissions` section
1. Click on the `Grant Moderation` button
1. Immediately the `Permissions` > `Admin?` section on the user's profile admin should read `Yes`

Note: we only have 15 Staff spots available with our current Discourse subscription. Please check with an [Admin](https://forum.gitlab.com/about) if you want to grant, or want to receive, moderator status.

### Deprovisioning

As a provisioner, follow these steps to deprovision a GitLab team member who is being offboarded:

1. Remove Admin and Moderator privileges
1. Reset trust level to 0 (new user)
1. Silence user (can't create new posts)
1. Unregister user (needs email confirmation to log in)
1. Suspend user (can't login)

### Moderator Specific Permissions

[Click this link to view the Moderator Quick Start guide.](https://forum.gitlab.com/t/read-me-moderator-quick-start-guide/39564)

This guide explains Moderator permissions, expectations, and best practices.

#### Moderator Specific Non-Technical Contributions

- Welcome new members
- Check forum threads for an existing answer: if there is an answer already, consider liking it or marking as solved
- Reassign categories on the forum
- Add or create appropriate tags
- Look in the forum for answers that may have already been supplied in an existing thread
- Look in the Support Zendesk instance for answers that may have already been supplied in a ticket from Support
- Ask the poster initial troubleshooting questions like screenshots, error messages, etc.

#### General Moderator Best Practices

- Leave all interactions and replies open-ended. We want our forum users to feel like they can always come back and pick up the conversation.
- [Consider gifting swag](/handbook/marketing/community-relations/code-contributor-program/community-appreciation/) to forum users that contribute a lot or in a really exceptional way! Do this via a DM/private message.

#### Moderator Only Access

Below is a list of actions only forum Moderators can take in the Discourse platform. For this reason, at this time only GitLab Staff members are granted Moderator status. Specifically those who are part of the Forum Contributors Group.

##### Marking accepted solutions

Mark a response as an "accepted solution" in a situation where a single reply effectively answers or resolves a forum thread. Only the original poster and moderators have the ability to mark an answer in a forum thread. Please do this freely! It helps with SEO, and it helps our users know what questions have been answered.

**Instructions**

Click the ellipsis in the small grey toolbar on the post > click the checkbox.

##### Editing topics and posts

Sometimes users will accidentally post private information like license or API keys, emails addresses, etc. Publicly identifiable information needs to be redacted from the forum. You can also use this tool to redact inappropriate language or other Code of Conduct violations.

**Instructions**

If you need to edit a user’s post for whatever reason, please private message the user in the forum letting them know that you have done so, and the reason why.

Click the ellipsis in the small grey toolbar on the post > click the pencil icon.

**Instructions For Sending a Private Message**

1. Click the user’s avatar icon, user card will appear
2. Click the `message` button on the user card to draft private message

##### Viewing email addresses

For the sake of anonymity, only Forum Moderators can see the email address associated with a forum user’s account.

**Instructions*

1. Click the user’s avatar icon, user card will appear
2. Click the avatar icon on the user card to navigate to the user’s profile
3. Click the `show` button in the user’s profile

##### Drafting in/using the private Staff Category

This is not exclusive to Moderators, but you can use the Staff Category to draft new knowledge share articles, initiatives, whatever you need! You will notice the little lock symbol next to private categories.

##### Deleting topics and posts

We do not delete topics and posts in the forum, mainly due to the fact that this action can destroy trust. There are always exceptions to this rule - a good example is when spam posts sneak in. If you feel something needs to be deleted, you can always reach out to [admins](https://forum.gitlab.com/about) in private or in the [#community-relations](https://gitlab.slack.com/archives/C0R04UMT9) Slack channel and we can talk it through!

**Instructions**

Click the ellipsis in the small grey toolbar on the post > click the trash can icon.

##### Moving Posts To a New Thread

It’s not uncommon that moderators will feel the need to move new posts out of old threads, or move posts to their own topic altogether. Please move new posts out of old topics when users try to bring topics a year or older back to life.

**Instructions**

1. Click wrench icon on right side of UI
2. Click `select post` in dropdown
3. Choose either `select+below` or `select` depending on how much of the conversation you want to move
4. Choose `move` to Fill out the popover accordingly
5. Click `move to new topic`

## Workflow

### General

#### How to respond in the GitLab Forum

1. Visit the [forum](https://forum.gitlab.com/) and navigate into the categories. Alternatively, use [New](https://forum.gitlab.com/new), [Latest](https://forum.gitlab.com/latest) and [Unread](https://forum.gitlab.com/unread) search filters.
2. Write a response if you can, or involve a team member for assistance on the issue if you don't know how to engage or proceed. Troubleshooting questions and asking for details counts as a reply! Don't feel like you have to answer fully in one go.

Consider using some of the following resources to help get answers to questions:

- Search the forum for related topics - you may find a community member who has already solved something similar. You can loop them into the conversation or point the user to a thread that might be helpful
- Search [GitLab's documentation](https://docs.gitlab.com) for helpful information to aid in troubleshooting
- [Search Support's Zendesk instance for solved tickets](/handbook/marketing/community-relations/community-advocacy/workflows/twitter/#using-gitlab-support-zendesk) about the same issue the forum user is having and share that knowledge in the forum thread

#### Working alongside the wider community

In the GitLab Community Forum, most tech support and troubleshooting assistance comes from community members volunteering their time and knowledge to help others.

If you see a community member volunteering their time to help another community member, give their reply a :heart: to show appreciation for their contribution.

#### Flagging Inappropriate Content

If any GitLab team member sees a forum post that is inappropriate, abusive, spam, or a violation of our code of conduct, please flag the post by clicking the flag icon. If immediate action to hide or redact private information is required, flag the post and then ping one of the [admins](https://forum.gitlab.com/about) in the [#community-relations](https://gitlab.slack.com/archives/C0R04UMT9) Slack channel.

### Moderator and Admin Workflow

**How and when to edit forum posts.**

Generally, we do not edit other peoples' posts. If we see something that can be improved, we should reach out to them privately to ask them to change it. This helps us build trust with our community and with the GitLab team, since it shows them that they have the creative freedom and autonomy to post what they like on our forum.

That being said, there are instances when moderators can edit posts directly. Here are some examples and how to do it:

- Violation of GitLab's [Code of Conduct](/company/culture/contribute/coc/) (Ex. redacting swear words, if they are unable or unwilling to edit it themselves)
- Redact private information (Ex. license keys, account info, email addresses, etc.)
- Editing can be achieved via the grey pencil icon in the post toolbar (... > pencil)

**How and when to mark a solution in a thread.**

- When there is a clear answer to the topic and/or question
- Marking the solution can be done via the grey checkbox icon in the post toolbar (... > checkbox)

**How and when to delete a forum post or topic.**

- Please do not delete other's posts. When necessary, message the user first, and let them have a chance to delete it themselves
- Deleting content for any reason is a breach of trust, which is something we are working hard to build in our forum community. If you feel like a post needs to be deleted, please reach out to an Admin, so the situation can be discussed.

Follow the relevant workflow depending on the question:

```mermaid
graph TD
A((Non-support)) --> B
B("Account") --> |Sales/Renewal| E["ping #sales"]
B --> |Other|F("Have they opened a support ticket?")
F --> |Yes, using GitLab.com| G["ping #support_gitlab-com"]
F --> |Yes, self-managed| H["ping #support_self-managed"]
F --> |No| I["Direct them to support.gitlab.com"]
A --> J("GitLab vs Competitor")
J --> |Question|K["Find answer via devops tools page/blog and link"]
J --> |Discussion|L["Link on #competition"]
A --> M("Feature Proposal")
M --> |Already exists|N["Link to the open issue"]
M --> |Doesn't exist|O["Ask them to open an issue"]
```

```mermaid
graph TD
A((Support)) --> B("What level of GitLab are they on?")
B --> |Not free|C("Have they opened a support ticket?")
C -->|No| E["Link them to the support portal"]
C -->|Yes| F("Have they provided the ticket number?")
F --> |No|R("Ask them for the ticket number")
R --> F
F --> |Yes|G("Self-Managed or .com?")
G --> |Self-Managed|H["ping #support_self-managed"]
G --> |.com|I["ping #support_gitlab-com"]
G --> |not provided|J("Ask what they're using")
J --> G
B-->|Free| D("Is there an issue open already?")
D --> |Yes|K["Link the issue"]
D --> |ask the following questions|L("Self-Managed or .com?")
L --> M("What version are they on?")
M --> N("Has this happened on a previous version?")
N --> O("How long have they been experiencing it?")
O --> P("What integrations do they have, if relevant?")
P --> |Questions answered|Q[ping relevant product channel]
```

## GitLab Forum Strategy

Engagement drives engagement, so the more the GitLab team engages with our wider community, the more they are likely to engage with us, and with others. By setting the example of providing thorough answers, we can build our forum up as a place of knowledge share and collaboration. [See the Forum's 2020 strategy slide deck for more](https://docs.google.com/presentation/d/1PiNlxFImSIO8kz9TfWMZ6GLGd9fYefILpC6LS3w3lJE/edit#slide=id.p).

### Connect with us via Slack

Join [#community-relations](https://gitlab.slack.com/archives/C0R04UMT9).
